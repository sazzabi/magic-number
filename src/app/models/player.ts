export class Player {
    name: string = '';
    code = ['-', '-', '-', '-'];
    score: number[] = [];

    public static new(name: string): Player {
        return <Player>{
            name: name,
            code: ['-', '-', '-', '-'],
            score: <number[]>[]
        }
    }
}
