import {Player} from './player';

export class Game {
    gameId: string = '9c7b5d7a-d931-4bed-8014-4d2ba14dcc23';
    players: Player[] = [
        <Player>{
            name: 'Joueur 1',
            code: ['-', '-', '-', '-'],
            score: <number[]>[]
        },
        <Player>{
            name: 'Joueur 2',
            code: ['-', '-', '-', '-'],
            score: <number[]>[]
        }
    ];

}
