import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';

import {HomepageComponentComponent} from './controllers/homepage-component.component';
import {NewGameComponent} from './controllers/new-game.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {GameComponent} from './controllers/game.component';
import {TypeSecretCodeComponent} from './components/type-secret-code.component';
import {DialerComponent} from './components/dialer.component';

import {GameManagerService} from './services/game-manager.service';
import {DialerButtonComponent} from './components/dialer-button.component';

@NgModule({
    declarations: [
        AppComponent,
        HomepageComponentComponent,
        NewGameComponent,
        GameComponent,
        TypeSecretCodeComponent,
        DialerComponent,
        DialerButtonComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        FormsModule,
        ReactiveFormsModule
    ],
    providers: [GameManagerService],
    bootstrap: [AppComponent]
})
export class AppModule {
}
