import {Injectable} from '@angular/core';
// @ts-ignore
import * as uuid from 'uuid';
import {Player} from '../models/player';
import {Game} from '../models/game';

@Injectable({
    providedIn: 'root'
})
export class GameManagerService {
    games: any = [];
    players: Player[] = [
        <Player>{
            name: 'Joueur 1',
            code: ['-', '-', '-', '-'],
            score: <number[]>[]
        },
        <Player>{
            name: 'Joueur 2',
            code: ['-', '-', '-', '-'],
            score: <number[]>[]
        }
    ];

    constructor() {

    }

    start(players: Player[]): string {
        let game = new Game();
        game.gameId = uuid.v4()
        game.players = players;
        this.games[game.gameId] = game;

        this.players = players;
        return game.gameId;
    }
}
