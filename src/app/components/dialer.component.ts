import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';

@Component({
    selector: 'app-dialer',
    templateUrl: './templates/dialer.component.html',
    styleUrls: []
})
export class DialerComponent implements OnInit {
    current: string[] = [];
    @Input()
    cursor: number = 0; // current digit being typed

    @Output() add = new EventEmitter<string>();

    @Output() erase = new EventEmitter();

    @Output() next = new EventEmitter();

    constructor() {
    }

    ngOnInit(): void {
    }

    addDigit(digit: string) {
        if (this.cursor < 4 && this.current.indexOf(digit) == -1) {
            this.current.push(digit);
            this.add.emit(digit);
        }
    }

    eraseDigit() {
        if (this.cursor > 0) {
            this.current.pop();
            this.erase.emit();
        }
    }

    nextStep() {
        if (this.cursor > 3) {
            this.current = [];
            this.next.emit();
        }
    }

    sleep(ms: number) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }

    async randomDigits() {
        while (this.current.length > 0) {
            this.eraseDigit();
        }
        await this.sleep(500);

        while (this.current.length < 4) {
            this.addDigit(this.randomNumber());
        }
    }

    randomNumber(): string {
        return Math.floor(Math.random() * 10) + '';
    }

}
