import {Component, OnInit, Input} from '@angular/core';

@Component({
    selector: 'app-type-secret-code',
    templateUrl: './templates/type-secret-code.component.html',
    styleUrls: []
})
export class TypeSecretCodeComponent implements OnInit {

    @Input()
    code = ['-', '-', '-', '-'];

    cursor: number = 0;

    constructor() {
    }

    ngOnInit(): void {
    }
}
