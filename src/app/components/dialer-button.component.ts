import {Component, OnInit, Output, EventEmitter, Input} from '@angular/core';

@Component({
    selector: 'app-dialer-button',
    templateUrl: './templates/dialer-button.component.html',
    styleUrls: []
})
export class DialerButtonComponent implements OnInit {
    @Input() digit: string = '';
    @Input() current: string[] = [];
    @Output() add = new EventEmitter<string>();

    constructor() {
    }

    ngOnInit(): void {
    }

    addDigit(digit: string) {
        this.add.emit(digit);
    }
}
