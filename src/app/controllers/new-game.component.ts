import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormArray, FormGroup} from '@angular/forms';
import {Router} from '@angular/router';
import {GameManagerService} from '../services/game-manager.service';
import {Player} from '../models/player';

@Component({
    selector: 'app-new-game',
    templateUrl: './templates/new-game.component.html',
    styleUrls: []
})
export class NewGameComponent implements OnInit {
    // @ts-ignore
    orderForm: FormGroup;
    // @ts-ignore
    items: FormArray;

    constructor(private formBuilder: FormBuilder,
                private router: Router,
                private gameManager: GameManagerService) {

    }

    ngOnInit() {
        this.orderForm = this.formBuilder.group({
            items: this.formBuilder.array([this.createItem('Joueur 1'), this.createItem('Joueur 2')])
        });
    }

    get orderItems() {
        return this.orderForm.get('items') as FormArray;
    }

    createItem(name: string = ''): FormGroup {
        return this.formBuilder.group({name: name});
    }

    start(): void {
        let players: Player[] = [];
        for (let i = 0; i < this.orderItems.value.length; i++) {
            players.push(Player.new(this.orderItems.value[i].name))
        }

        const id = this.gameManager.start(players);
        this.router.navigate(['/game', id]);
    }
}
