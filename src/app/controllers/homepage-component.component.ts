import {Component, OnInit} from '@angular/core';

@Component({
    selector: 'app-homepage-component',
    templateUrl: './templates/homepage-component.component.html',
    styleUrls: []
})
export class HomepageComponentComponent implements OnInit {

    //TODO: add help
    //TODO: Tech: Add translation
    //TODO: Tech: store game data by id { 'id1':[players...]; 'id2': [players...]} somewhere (local storage, indexedDb, webSQL...)
    //TODO: Feature: add "end game" button when last player wins
    //TODO: Feature: Alternate which player starts first ? (1,2 then 2,1 ...)

    //TODO: Feature: game modes:
    //          Local Players (local 1 vs 1)            => Done
    //          Single Player (local 1 vs computer)
    //          Private Network (invite by link)
    //          Online (find opponent)

    //TODO: Feature: add game mode: Single player (random numbers)
    //TODO: Feature: add game mode: Network game (invite by link)
    //TODO: Feature: add game mode: Online (find opponent)

    //TODO: add game levels:
    //          Chill: no timer
    //          Hard: timer (either player have limited time, 30s for example, or add total time to score)
    //          Nightmare: Show colors not on numbers
    //          Hell ????

    constructor() {
    }

    ngOnInit(): void {
    }

}
