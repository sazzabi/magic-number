import {Component, OnInit} from '@angular/core';
import {GameManagerService} from '../services/game-manager.service';
import {Player} from '../models/player';

@Component({
    selector: 'app-game',
    templateUrl: './templates/game.component.html',
    styleUrls: []
})
export class GameComponent implements OnInit {
    // @ts-ignore
    players: Player[];
    turn: number = 0; // current player
    step: number = 0; // 0: choose, 1: guess
    cursor: number = 0; // current digit being typed
    tries: any = [];
    currentTry: string[] = ['-', '-', '-', '-'];
    numberGuessed = false;

    constructor(public gameManager: GameManagerService) {
    }

    ngOnInit(): void {
        this.players = this.gameManager.players;
    }

    addDigit(digit: string): void {
        if (this.step === 0) {
            this.gameManager.players[this.turn].code[this.cursor] = digit;
            this.cursor++;
        } else {
            this.currentTry[this.cursor] = digit;
            this.cursor++;
        }
    }

    eraseDigit(): void {
        if (this.step === 0) {
            this.cursor--;
            this.gameManager.players[this.turn].code[this.cursor] = '-';
        } else {
            this.cursor--;
            this.currentTry[this.cursor] = '-';
        }
    }

    nextStep(): void {
        // Done typing
        if (this.step === 0) {
            // Typing secret code
            this.step = 1;
            this.cursor = 0;
        } else {
            // Guessing secret code
            this.checkNumber();
            if (this.numberGuessed) {
                this.turn = (this.turn + 1) % 2;
                this.step = 0;
                this.cursor = 0;
            }
        }
    }

    private checkNumber() {
        let secret: string[] = Object.assign([], this.gameManager.players[this.turn].code);

        let item = [
            {digit: this.currentTry[0], state: ''},
            {digit: this.currentTry[1], state: ''},
            {digit: this.currentTry[2], state: ''},
            {digit: this.currentTry[3], state: ''}
        ];

        let successCount = 0;
        for (let i = 0; i < 4; i++) {
            if (item[i].digit === secret[i]) {
                item[i].state = 'success';
                secret[i] = '';
                successCount++;
            }
        }

        for (let i = 0; i < 4; i++) {
            if (item[i].digit !== secret[i] && secret[i] !== '') {
                item[i].state = secret.indexOf(item[i].digit) > -1 ? 'danger' : 'light';
            }
        }

        this.tries.push(item);
        this.numberGuessed = successCount === 4;
        if (this.numberGuessed) {
            this.gameManager.players[(this.turn + 1) % 2].score.push(this.tries.length);
        }
        this.currentTry = ['-', '-', '-', '-'];
        this.cursor = 0;
    }

    newGampe() {
        this.step = 0;
        this.numberGuessed = false;
        this.tries = [];
        this.cursor = 0;
        this.currentTry = ['-', '-', '-', '-'];
        for (let i = 0; i < this.gameManager.players.length; i++) {
            this.gameManager.players[i].code = ['-', '-', '-', '-'];
        }
    }

    getTotal(score: number[]) {
        return score.length > 0 ? score.reduce((previousValue: number, currentValue: number) => previousValue + currentValue) : 0;
    }
}
