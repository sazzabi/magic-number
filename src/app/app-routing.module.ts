import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomepageComponentComponent} from './controllers/homepage-component.component';
import {NewGameComponent} from './controllers/new-game.component';
import {GameComponent} from './controllers/game.component';

const routes: Routes = [
    {
        path: '',
        component: HomepageComponentComponent
    },
    {
        path: 'new',
        component: NewGameComponent
    },
    {
        path: 'game/:code',
        component: GameComponent
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
